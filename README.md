# Web application to upload a file to Oracle Cloud Bucket

## Description
Web application to upload File using Python/Flask to Oracle Cloud Bucket using AWS Python library (boto3).

Reference:
- Refer [README.md](web/README.md) inside web/ directory for detailed steps