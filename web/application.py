import os
import logging
from flask import Flask, flash, request, redirect
from flask import render_template
from werkzeug.utils import secure_filename
import boto3 as boto3
from botocore.exceptions import ClientError

# **********************************************************************************
# IMPORTANT NOTE
# Source of following set of information is .env file
# Refer the sample file and set the variables correctly before starting application
# **********************************************************************************
FLASK_APP = os.environ.get("FLASK_APP")
FLASK_ENV = os.environ.get("FLASK_ENV")
UPLOAD_FOLDER = os.environ.get("UPLOAD_FOLDER")
S3_BUCKET_NAME = os.environ.get("S3_BUCKET_NAME")
OCI_REGION=os.environ.get("OCI_REGION")
AWS_SECRET_ACCESS_KEY=os.environ.get("AWS_SECRET_ACCESS_KEY")
AWS_ACCESS_KEY_ID=os.environ.get("AWS_ACCESS_KEY_ID")
BUCKET_ENDPOINT_URL=os.environ.get("BUCKET_ENDPOINT_URL")


s3 = boto3.resource(
    "s3",
    region_name=OCI_REGION,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    endpoint_url=BUCKET_ENDPOINT_URL
   )


# Local Variables
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'super_secret_key'
ALLOWED_EXTENSIONS = {'txt', 'yaml'}


def S3_upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    #  If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name


    #  Upload the file

    #s3_client = boto3.client("s3")
    try:
        #response = s3_client.upload_file(file_name, bucket, object_name)
        # Upload a File to you OCI Bucket, 2nd value is your bucket name
        s3.meta.client.upload_file(file_name, bucket, object_name)

        logging.info("Successfully uploaded a file to S3 bucket")
    except ClientError as e:
        logging.error(e)
        return False
    return True


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/uploads/<filename>')
def uploaded_file(filename, status=None):
    return render_template("index.html", stauts=status)
    
@app.route('/index',methods=['GET'])
def index():
    return 'S3_BUCKET_UPLOADER'

@app.route('/', methods=['GET', 'POST'])
@app.route('/<status>',methods=['GET','POST'])
def upload_file(status=None):
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_and_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_and_path)
            S3_upload_file(file_and_path, S3_BUCKET_NAME)
            return redirect(request.url)
    return render_template('index.html', status=status)