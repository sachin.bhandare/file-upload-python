# Steps to run on Linux server

Below are the steps required to get this working on a base linux system.

- Install python3 on linux server
- Install all required dependencies
- Configure Web Server
- Start Web Server

## 1. Install python3 on Oracle Linux

```bash
sudo yum install -y python37
sudo pip3 install boto3
sudo pip3 install awscli
```

## 2. Install all required dependencies
Change directory to web and run following command.

```bash
cd web 
pip3 install -r requirements.txt
```

### 2.1 Optionally create a virtual environment using following steps

## Python3 Virtualenv Setup

##### Requirements
* Python 3
* Pip 3

```bash
$ brew install python3
```

Pip3 is installed with Python3

##### Installation
To install virtualenv via pip run:
```bash
$ pip3 install virtualenv
```

##### Usage
Creation of virtualenv:
```bash
$ virtualenv -p python3 <desired-path>
```

Activate the virtualenv:
```bash
$ source <desired-path>/bin/activate
```

Deactivate the virtualenv:
```bash
$ deactivate
```


[About Virtualenv](https://virtualenv.pypa.io/en/stable/)

## 3. Create/update environment using .env file
Make a copy of env.sample into .env file. Update the environment variable before starting the server.

```bash
cd web
cp env.sample .env
```

## 4. Start the flask server using flask run

```bash
cd web
flask run
```

References:
- [GitHub](https://github.com/pallets/flask/blob/0c0b31a789f8bfeadcbcf49d1fb38a00624b3065/src/flask/app.py#L925)
- [Using boto2 to upload file to OCI](https://medium.com/oracledevs/use-aws-python-sdk-to-upload-files-to-oracle-cloud-infrastructure-object-storage-b623e5681412)
