#!/bin/bash

export FLASK_APP=application.py
export FLASK_ENV=development

#TODO: Check if the flask is installed
# If flask is not installed then install using following command
# $ pip install flask

flask run --host=0.0.0.0 --port=8080